// const { read } = require('fs')
const HTTP = require('http')
const PORT = 4000

HTTP.createServer((req,res) => {
    if(req.url==="/" && req.method=="GET"){
        res.writeHead(200, {"Content-type": "text/plain"})
        res.write("Welcome to Booking System")
        res.end()
    }
    else if(req.url==="/profile" && req.method=="GET"){
        res.writeHead(200, {"Content-type": "text/plain"})
        res.write("Welcome to your profile!")
        res.end()
    }
    else if(req.url==="/courses" && req.method=="GET"){
        res.writeHead(200, {"Content-type": "text/plain"})
        res.write("Here's our courses available")
        res.end()
    }
    else if(req.url==="/addcourse" && req.method=="POST"){
        res.writeHead(200, {"Content-type": "text/plain"})
        res.write("Add a course to our resources")
        res.end()
    }
    else if(req.url==="/updatecourse" && req.method=="PUT"){
        res.writeHead(200, {"Content-type": "text/plain"})
        res.write("Update a course to our resources")
        res.end()
    }
    else if(req.url==="/archivecourses" && req.method=="DELETE"){
        res.writeHead(200, {"Content-type": "text/plain"})
        res.write("Archive courses to our resources")
        res.end()
    }
}).listen(PORT, ()=>console.log(`Connected to PORT ${PORT}`))